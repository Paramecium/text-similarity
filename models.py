from pydantic import BaseModel

# ----------- Сравнение текстов -----------
class SimilarityRequest(BaseModel):
    Sentences: list[str]

class SimilarityResponse(BaseModel):
    Similarity: list[float]

# ----- Получение эмбеддингов текстов ----- 
class EmbeddingsRequest(BaseModel):
    Sentences: list[str]

class EmbeddingsResponse(BaseModel):
    Embeddings: list[list[float]]

    # ----------- Сравнение текстов -----------
class SimilarityByEmbeddingsRequest(BaseModel):
    Embeddings: list[list[float]]

class SimilarityByEmbeddingsResponse(BaseModel):
    Similarity: list[float]


