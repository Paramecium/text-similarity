from transformers import AutoTokenizer, AutoModel
import torch

def load_tokenizer():
    tokenizer = AutoTokenizer.from_pretrained("symanto/sn-xlm-roberta-base-snli-mnli-anli-xnli")
    return tokenizer

def load_model():
    model = AutoModel.from_pretrained('symanto/sn-xlm-roberta-base-snli-mnli-anli-xnli')
    return model