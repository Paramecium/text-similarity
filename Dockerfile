# start by pulling the python image
FROM python:3.9-slim-buster

RUN pip install --upgrade pip

# switch working directory
WORKDIR /app_network

# copy the requirements file into the image
COPY ./requirements.txt /app_network/requirements.txt

# install the dependencies and packages in the requirements file
RUN pip install -r requirements.txt

# copy every content from the local file to the image
COPY . /app_network

# configure the container to run in an executed manner
# ENTRYPOINT [ "python" ]

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "80"]
