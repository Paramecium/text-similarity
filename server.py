import os

from numpy import dot, ndarray
from numpy.linalg import norm
import torch

import uvicorn

from fastapi import FastAPI
from fastapi import Response
from fastapi import Query

from loader import load_tokenizer, load_model
from models import SimilarityRequest, SimilarityResponse, EmbeddingsRequest, EmbeddingsResponse, SimilarityByEmbeddingsRequest, SimilarityByEmbeddingsResponse


def mean_pooling(model_output, attention_mask):
    token_embeddings = model_output[0]
    input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
    return torch.sum(token_embeddings * input_mask_expanded, 1) / torch.clamp(input_mask_expanded.sum(1), min=1e-9)


def calc_similarity(
        embeddings: ndarray):
    if len(embeddings) < 1:
        return []
    else:
        result = []
        support = embeddings[0]
        support_norm = norm(support)
        for item in embeddings:
            result.append(float(dot(item, support) / (support_norm * norm(item))))
        return result


app = FastAPI(title='TextSimilarity')
model = load_model()
tokenizer = load_tokenizer()

@app.post(
    '/similarity', 
    tags = ['Similarity'],
    summary = 'Получение меры схожести текcтов относительно первого',
    response_model=SimilarityResponse)
def news_similarity(
    request: SimilarityRequest):
    encoded_input = tokenizer(request.Sentences, padding='max_length', truncation=True, return_tensors='pt')
    with torch.no_grad():
        model_output = model(**encoded_input)
    sentence_embeddings = mean_pooling(model_output, encoded_input['attention_mask'])
    response = {"Similarity": calc_similarity(torch.Tensor.numpy(sentence_embeddings))}

    return response;

@app.post(
    '/get-embeddings', 
    tags = ['Similarity'],
    summary = 'Получение эмбеддингов текстов',
    response_model=EmbeddingsResponse)
def news_embeddings(
    request: EmbeddingsRequest):
    encoded_input = tokenizer(request.Sentences, padding=True, truncation=True, return_tensors='pt')
    with torch.no_grad():
        model_output = model(**encoded_input)
    sentence_embeddings = mean_pooling(model_output, encoded_input['attention_mask'])

    response = {"Embeddings": sentence_embeddings.tolist()}

    return response;

@app.post(
    '/similarity-by-embeddings', 
    tags = ['Similarity'],
    summary = 'Получение меры схожести текстов по их эмбеддингам. Первый элемент - основа для сравнения',
    response_model=SimilarityByEmbeddingsResponse)
def similarity_by_embeddings(
    request: SimilarityByEmbeddingsRequest):

    response = {"Similarity": calc_similarity(request.Embeddings)}
    
    return response;


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 80))
    uvicorn.run('main:app', host="127.0.0.1", port=port, reload=True)
